<?php 

  //  adding js & css files

  function modern_coding_setup(){
    
    wp_enqueue_style(
      'fontawesome',  // alias
      get_stylesheet_directory_uri().'/fontawesome/css/all.min.css'
    );

    wp_enqueue_style(
      'bootstrap',  // alias
      get_stylesheet_directory_uri().'/bootstrap/dist/css/bootstrap.min.css'
    );

  }


  add_action(
    'wp_enqueue_scripts',   // alias
    'modern_coding_setup'  // name of the function to run
  );


  //  adding theme support

  function modern_coding_init() {
    
    add_theme_support('post-thumbnails');
    add_theme_support('title-tag');
    
    add_theme_support(
      'html5',
      array('comment-list', 'comment-form', 'search-form')
    );
  
  }


  add_action(
    'after_setup_theme',   // alias
    'modern_coding_init'  // name of the function to run
  );

?>