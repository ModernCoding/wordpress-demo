<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="UTF-8">
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0"
    >
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <?php wp_head(); ?>

    <link
      rel="stylesheet"
      type="text/css"
      href="<?php echo get_template_directory_uri(); ?>/style.css"
    >

    <script
      defer
      type="text/javascript"
      
      src="
          <?php
            echo get_template_directory_uri();
          ?>/js/jquery.js
        "
    ></script>

    <script
      defer
      type="text/javascript"
      
      src="
          <?php
            echo get_template_directory_uri();
          ?>/js/modernizr.mq.min.js
        "
    ></script>

    <script
      defer
      type="text/javascript"
      
      src="
          <?php
            echo get_template_directory_uri();
          ?>/js/imagesloaded.min.js
        "
    ></script>

    <script
      defer
      type="text/javascript"
      
      src="
          <?php
            echo get_template_directory_uri();
          ?>/js/stimulus.umd.js
        "
    ></script>

    <script
      defer
      type="text/javascript"
      
      src="
          <?php
            echo get_template_directory_uri();
          ?>/js/turbolinks.js
        "
    ></script>

    <script
      defer
      type="text/javascript"
      
      src="
          <?php
            echo get_template_directory_uri();
          ?>/js/turbolinksStart.js
        "
    ></script>

    <script
      defer
      type="text/javascript"
      
      src="
          <?php
            echo get_template_directory_uri();
          ?>/js/controllers/app_controller.js
        "
    ></script>

    <script
      defer
      type="text/javascript"
      
      src="
          <?php
            echo get_template_directory_uri();
          ?>/js/controllers/menu_controller.js
        "
    ></script>

    <script
      defer
      type="text/javascript"
      
      src="
          <?php
            echo get_template_directory_uri();
          ?>/js/controllers/post_controller.js
        "
    ></script>
  </head>


  <body data-controller="app">

    <header>
      <div id="banner" class="text-center">
        <img
          src="
            <?php
              echo get_template_directory_uri();
            ?>/img/ModernCoding-Horizontal.png
          "
        >
      </div>

      <div 
        class="d-flex flex-wrap justify-content-center"
        data-controller="menu"
        data-menu-pathname="/"
          
      >
        <a
          href="<?php echo site_url('') ?>"
          class="btn btn-sm btn-outline-light"
          data-pathname="/"
        >
          Home
        </a>

        <a
          href="<?php echo site_url('/about') ?>"
          class="btn btn-sm btn-outline-light"
          data-pathname="/about"
        >
          About myself
        </a>
        
      </div>
    </header>


    <main>
