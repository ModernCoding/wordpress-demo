(() => {
  const application = Stimulus.Application.start()

  application.register("menu", class extends Stimulus.Controller {

    connect() {
      this.data.set("pathname", window.location.pathname);
      let pathname = this.data.get("pathname");

      $('[data-pathname]').each(function(){

        switch (true) {

          case pathname == "/" && $(this).attr('data-pathname') == "/":
            
            $(this)
              .addClass('btn-warning')
              .removeClass('btn-outline-light');

            break;


          case pathname == "/" && $(this).attr('data-pathname') != "/":
          case $(this).attr('data-pathname') == "/":
            
            $(this)
              .removeClass('btn-warning')
              .addClass('btn-outline-light');

            break;


          case pathname.includes($(this).attr('data-pathname')):
            
            $(this)
              .addClass('btn-warning')
              .removeClass('btn-outline-light');

            break;


          default:
            
            $(this)
              .removeClass('btn-warning')
              .addClass('btn-outline-light');

            break;

        }

      });
      
    }

  })
})()