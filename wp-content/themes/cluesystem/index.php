<?php get_header(); ?>

<h2 class="font-weight-bold">
  <u>All posts</u>
</h2>

<article data-controller="post">

  <div data-target="post.detail" class="d-none">
  </div>

  <?php

    while ( have_posts() ):
      the_post();
  
      if (get_the_category(get_the_ID())[0]->name == "Clue System"):
  ?>

        <h3 
          data-url="<?php echo the_permalink(); ?>"
          data-action="click->post#show"
        >
          <?php echo the_title(); ?>
        </h3>

  <?php
      endif;
      
    endwhile;
  ?>
</article>


<?php get_footer(); ?>
