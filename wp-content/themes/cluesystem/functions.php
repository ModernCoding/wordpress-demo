<?php 

  //  adding theme support

  function clue_system_init() {
    
    add_theme_support('post-thumbnails');
    add_theme_support('title-tag');
    
    add_theme_support(
      'html5',
      array('comment-list', 'comment-form', 'search-form')
    );
  
  }


  add_action(
    'after_setup_theme',   // alias
    'clue_system_init'  // name of the function to run
  );

?>