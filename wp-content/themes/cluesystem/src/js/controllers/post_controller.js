import { Controller } from "stimulus";
import { resize } from "../resizing";

export default class extends Controller {

  static targets = [ "detail" ];


  show(e) {

    let $post = $(e.target);

    fetch($post.attr("data-url"))
      .then(response => response.text())
      
      .then(html => {
        $(this.detailTarget)
          .removeClass('d-none')
          .html(html);

        $('[data-controller="post"]')
          .find('h3')
          .removeClass('d-none');
        
        $post.addClass('d-none');
        $('html, body').animate({ scrollTop: 0}, 600);
        resize();
      });
  
  }


  hide() {
    $(this.detailTarget)
      .addClass('d-none')
      .html(null);

    $('[data-controller="post"]')
      .find('h3')
      .removeClass('d-none');
        
    resize();
  }

}
