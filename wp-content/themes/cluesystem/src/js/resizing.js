$(document).on('turbolinks:load', resize);
$(window).resize(resize);

export function resize() {

  let $aside = $('aside');
  let $main = $('main');

  $('html').imagesLoaded(() => {
    
    $main.css('min-height', '100vh');
    $aside.css('height', 'auto');
    $('.my-aside-button').css('width', 'auto');


    if (Modernizr.mq('(min-width: 768px)')) {
      $aside.css('height', $('main').css('height'));
      $('.my-aside-button').css('width', '100%');
    }


    else {

      let myAsideButtonWidth = 0;
      let asideHeight = parseFloat($aside.css('height'));

      $('.my-aside-button').each(function(){
        let thisWidth = parseFloat($(this).css('width'));

        if (thisWidth > myAsideButtonWidth) {
          myAsideButtonWidth = thisWidth;
        }
      });


      $('.my-aside-button').css('width', `${myAsideButtonWidth}px`);
      $main.css('min-height', `-=${asideHeight}px`);
      
    }

  });

}
