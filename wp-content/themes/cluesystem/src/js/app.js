import "../stylesheets/app.sass";


//  [theme]/node_modules/
require("bootstrap");

window.$ = require('jquery');
var imagesLoaded = require('imagesloaded');
var Turbolinks = require('turbolinks');

import { Application } from "stimulus";
import { definitionsFromContext } from "stimulus/webpack-helpers";


//  [theme]/src/js/*.js
require('./modernizr.mq.min');
require('./resizing');


imagesLoaded.makeJQueryPlugin($);
Turbolinks.start();

const application = Application.start();
const context = require.context("./controllers", true, /\.js$/);
application.load(definitionsFromContext(context));