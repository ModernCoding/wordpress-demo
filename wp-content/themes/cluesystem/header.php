<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="UTF-8">
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0"
    >
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <?php wp_head(); ?>
    
    <link
      rel="stylesheet"
      type="text/css"
      href="<?php echo get_template_directory_uri(); ?>/style.css"
    >

    <script
      defer
      type="text/javascript"
      
      src="
          <?php
            echo get_template_directory_uri();
          ?>/dist/app.js
        "
    ></script>
  </head>


  <body data-controller="app">

    <section class="container-fluid">
      <div class="row">
        
        <aside
          data-controller="menu"
          data-menu-pathname="/"
          class="col-md-4 col-lg-3 col-xl-2"
        >
          <div
            class="d-flex flex-wrap align-items-center d-md-block"
          >
            
            <div id="my-logo">
              <img>
            </div>

            <div
              class="d-flex flex-wrap align-items-center d-md-block"
            >
              
              <div class="my-aside-button">
                <a
                  href="<?php echo site_url('') ?>"
                  class="btn btn-sm btn-outline-light"
                  data-pathname="/"
                >
                  Home
                </a>
              </div>
              
              <div class="my-aside-button">
                <a
                  href="<?php echo site_url('/about') ?>"
                  class="btn btn-sm btn-outline-light"
                  data-pathname="/about"
                >
                  About myself
                </a>
              </div>

            </div>

          </div>
        </aside>


        <main role="main" class="col">