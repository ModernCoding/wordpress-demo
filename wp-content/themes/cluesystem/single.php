<?php

  while( have_posts() ) {
    the_post();

?>

  <h3 class="font-weight-bold text-warning">
    <?php echo the_title(); ?>
  </h3>

  <article>

    <img
      src="
          <?php
            echo get_the_post_thumbnail_url(get_the_ID());
          ?>
        "

      alt="Card Image"
    >

    <p>
      <?php echo get_the_excerpt(); ?>
    </p>

    <h4 
      class="font-weight-bold text-danger"
      data-action="click->post#hide"
    >
      Hide
    </h4>

  </article>

<?php
  }
?>
