const path = require("path");
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');


module.exports = {
  entry: {
    main: "./src/js/app.js"
  },

  output: {
    filename: "app.js",
    path: path.resolve(__dirname, "dist")
  },

  module: {
    rules: [
      {
        test: /\.(css|sass|scss)$/i,

        use: [
          MiniCssExtractPlugin.loader, //  3.  inject styles into dom
          'css-loader',   //  2.  turns css into cjs
          'sass-loader'   //  1.  turns sass into css
        ],
      },

      {
        test: /\.(svg|png|jpg|gif)$/i,

        use: {
          loader: 'file-loader',
          options: {
            name: "[name].[ext]",
            outputPath: "img",
            publicPath: "./dist/img"
          }
        }
      },

      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,

        use: {
          loader: 'file-loader',
          options: {
            name: "[name].[ext]",
            outputPath: "fonts",
            publicPath: "./dist/fonts"
          }
        }
      },

      {
        test: /\.js/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              '@babel/preset-env'
            ],
            plugins: [
              '@babel/plugin-proposal-class-properties'
            ]
          }
        }
      }
    ]
  },

  plugins: [
    new CleanWebpackPlugin(),

    new MiniCssExtractPlugin({
      filename: '../style.css'
    })
  ]
}